rome tel.search.ch module
=========================

Rome module for tel.search.ch
see http://tel.search.ch/api/help.en.html

Don't forget to:
1) Ask for an api key
2) Respect the term of use of tel.search.ch (see http://tel.search.ch/api/terms.en.html)

To use this module, see Rome documentation to add Rome modules.
In properties directory, you can find this module declaration.


rome
====

ROME is a set of RSS and Atom Utilities for Java. It makes it easy to work in Java with most syndication formats: RSS 0.90, RSS 0.91 Netscape, RSS 0.91 Userland, RSS 0.92, RSS 0.93, RSS 0.94, RSS 1.0, RSS 2.0, Atom 0.3, Atom 1.0

More Information: http://rometools.github.io/rome-modules/
