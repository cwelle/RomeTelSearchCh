package com.rometools.modules.searchch;

import org.eclipse.jdt.annotation.NonNull;

/**
 * tel.search.ch constants.
 *
 * @author Welle Charlotte
 */
class SearchCHConstants {

    /**
     * {@value}
     */
    static final @NonNull String CANTON = "canton";
    /**
     * {@value}
     */
    static final @NonNull String CATEGORY = "category";
    /**
     * {@value}
     */
    static final @NonNull String CITY = "city";
    /**
     * {@value}
     */
    static final @NonNull String EMAIL = "email";
    /**
     * {@value}
     */
    static final @NonNull String EXTRA = "extra";
    /**
     * {@value}
     */
    static final @NonNull String FAX = "fax";
    /**
     * {@value}
     */
    static final @NonNull String FIRSTNAME = "firstname";
    /**
     * {@value}
     */
    static final @NonNull String ID = "id";
    /**
     * {@value}
     */
    static final @NonNull String MAIDENNAME = "maidenname";
    /**
     * {@value}
     */
    static final @NonNull String MOBILE = "mobile";
    /**
     * {@value}
     */
    static final @NonNull String NAME = "name";
    /**
     * {@value}
     */
    static final @NonNull String NOPROMO = "nopromo";
    /**
     * {@value}
     */
    static final @NonNull String OCCUPATION = "occupation";
    /**
     * {@value}
     */
    static final @NonNull String ORG = "org";
    /**
     * {@value}
     */
    static final @NonNull String PHONE = "phone";
    /**
     * {@value}
     */
    static final @NonNull String POBOX = "pobox";
    /**
     * {@value}
     */
    static final @NonNull String POS = "pos";
    /**
     * {@value}
     */
    static final @NonNull String PUBLISHED = "published";
    /**
     * {@value}
     */
    static final @NonNull String SKYPE = "skype";
    /**
     * {@value}
     */
    static final @NonNull String STREET = "street";
    /**
     * {@value}
     */
    static final @NonNull String STREETNO = "streetno";
    /**
     * {@value}
     */
    static final @NonNull String SUBNAME = "subname";
    /**
     * {@value}
     */
    static final @NonNull String TITLE = "title";
    /**
     * {@value}
     */
    static final @NonNull String TYPE = "type";
    /**
     * {@value}
     */
    static final @NonNull String UPDATED = "updated";
    /**
     * {@value}
     */
    static final @NonNull String WEBSITE = "website";
    /**
     * {@value}
     */
    static final @NonNull String ZIP = "zip";

}
