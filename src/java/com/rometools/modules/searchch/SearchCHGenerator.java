package com.rometools.modules.searchch;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.rometools.modules.searchch.interfaces.SearchCH;
import com.rometools.rome.feed.module.Module;
import com.rometools.rome.io.ModuleGenerator;

/**
 * tel.search.ch generator
 *
 * @author Charlotte
 */
public class SearchCHGenerator implements ModuleGenerator {

    private static final Namespace NAMESPACE = Namespace.getNamespace("tel", SearchCH.URI);
    private static final Set<Namespace> NAMESPACES;

    static {
        final Set<Namespace> namespaces = new HashSet<>();
        namespaces.add(NAMESPACE);
        NAMESPACES = Collections.unmodifiableSet(namespaces);
    }

    @Override
    public String getNamespaceUri() {
        return SearchCH.URI;
    }

    @Override
    public Set<Namespace> getNamespaces() {
        return NAMESPACES;
    }

    // Implements the module generation logic
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    @Override
    public void generate(final Module module, final Element element) {
        final SearchCH myModule = (SearchCH) module;
        if (myModule.getName() != null) {
            final Element myElement = new Element(SearchCHConstants.NAME, NAMESPACE);
            myElement.setText(myModule.getName());
            element.addContent(myElement);
        }
        if (myModule.getZip() != null) {
            final Element myElement = new Element(SearchCHConstants.ZIP, NAMESPACE);
            myElement.setText(myModule.getZip());
            element.addContent(myElement);
        }
    }
}
