package com.rometools.modules.searchch;

import java.time.LocalDate;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import com.rometools.modules.searchch.interfaces.SearchCH;
import com.rometools.rome.feed.CopyFrom;
import com.rometools.rome.feed.module.ModuleImpl;

/**
 * SearchCh implementation module.
 *
 * @author Welle Charlotte
 */
public class SearchCHImpl extends ModuleImpl implements SearchCH {

    private static final long serialVersionUID = -8275118704842545845L;

    private @Nullable String zip;
    private @Nullable String name;
    private int pos = -1;
    private int id = -1;
    private @Nullable String type;
    private @Nullable String firstName;
    private @Nullable String subName;
    private @Nullable String maiden;
    private @Nullable String occupation;
    private @Nullable List<@NonNull String> categories;
    private @Nullable String street;
    private @Nullable String number;
    private @Nullable String box;
    private @Nullable String city;
    private @Nullable String canton;
    private @Nullable String noPromo;
    private @Nullable String phone;
    private @Nullable String fax;
    private @Nullable String email;
    private @Nullable String website;
    private @Nullable String skype;
    private @Nullable String mobile;
    private @Nullable LocalDate publishedDate;
    private @Nullable LocalDate lastUpdated;
    private @Nullable String organizationName;
    private @Nullable String title;

    /**
     * Constructor.
     */
    public SearchCHImpl() {
        super(SearchCH.class, SearchCH.URI);
    }

    @Override
    public Class<SearchCH> getInterface() {
        return SearchCH.class;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getZip() {
        return this.zip;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public void setZip(final String zip) {
        this.zip = zip;
    }

    @Override
    public void copyFrom(final CopyFrom obj) {
        final SearchCH module = (SearchCH) obj;
        setName(module.getName());
        setZip(module.getZip());
    }

    @Override
    public int getPos() {
        return this.pos;
    }

    @Override
    public void setPos(final int pos) {
        this.pos = pos;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public void setId(final int id) {
        this.id = id;
    }

    @Override
    public @Nullable String getType() {
        return this.type;
    }

    @Override
    public void setType(@Nullable final String type) {
        this.type = type;
    }

    @Override
    public @Nullable String getFirstName() {
        return this.firstName;
    }

    @Override
    public void setFirstName(@Nullable final String firstName) {
        this.firstName = firstName;
    }

    @Override
    public @Nullable String getSubName() {
        return this.subName;
    }

    @Override
    public void setSubName(@Nullable final String subName) {
        this.subName = subName;
    }

    @Override
    public @Nullable String getMaidenName() {
        return this.maiden;
    }

    @Override
    public void setMaidenName(@Nullable final String maiden) {
        this.maiden = maiden;
    }

    @Override
    public @Nullable String getOccupation() {
        return this.occupation;
    }

    @Override
    public void setOccupation(@Nullable final String occupation) {
        this.occupation = occupation;
    }

    @Override
    public @Nullable List<@NonNull String> getCategories() {
        return this.categories;
    }

    @Override
    public void setCategories(@Nullable final List<@NonNull String> categories) {
        this.categories = categories;
    }

    @Override
    public @Nullable String getStreet() {
        return this.street;
    }

    @Override
    public void setStreet(@Nullable final String street) {
        this.street = street;
    }

    @Override
    public @Nullable String getStreetNumber() {
        return this.number;
    }

    @Override
    public void setStreetNumber(@Nullable final String number) {
        this.number = number;
    }

    @Override
    public @Nullable String getPOBox() {
        return this.box;
    }

    @Override
    public void setPOBox(@Nullable final String box) {
        this.box = box;
    }

    @Override
    public @Nullable String getCity() {
        return this.city;
    }

    @Override
    public void setCity(@Nullable final String city) {
        this.city = city;
    }

    @Override
    public @Nullable String getCanton() {
        return this.canton;
    }

    @Override
    public void setCanton(@Nullable final String canton) {
        this.canton = canton;
    }

    @Override
    public @Nullable String getNoPromo() {
        return this.noPromo;
    }

    @Override
    public void setNoPromo(@Nullable final String noPromo) {
        this.noPromo = noPromo;
    }

    @Override
    public @Nullable String getPhone() {
        return this.phone;
    }

    @Override
    public void setPhone(@Nullable final String phone) {
        this.phone = phone;
    }

    @Override
    public @Nullable String getFax() {
        return this.fax;
    }

    @Override
    public void setFax(@Nullable final String fax) {
        this.fax = fax;
    }

    @Override
    public @Nullable String getEmail() {
        return this.email;
    }

    @Override
    public void setEmail(@Nullable final String email) {
        this.email = email;
    }

    @Override
    public @Nullable String getWebSite() {
        return this.website;
    }

    @Override
    public void setWebSite(@Nullable final String website) {
        this.website = website;
    }

    @Override
    public @Nullable String getSkype() {
        return this.skype;
    }

    @Override
    public void setSkype(@Nullable final String skype) {
        this.skype = skype;
    }

    @Override
    public @Nullable String getMobile() {
        return this.mobile;
    }

    @Override
    public void setMobile(@Nullable final String mobile) {
        this.mobile = mobile;
    }

    @Override
    public @Nullable String getTitle() {
        return this.title;
    }

    @Override
    public void setTitle(@Nullable final String title) {
        this.title = title;
    }

    @Override
    public @Nullable LocalDate getPublished() {
        return this.publishedDate;
    }

    @Override
    public void setPublished(@Nullable final LocalDate date) {
        this.publishedDate = date;
    }

    @Override
    public @Nullable LocalDate getLastUpdated() {
        return this.lastUpdated;
    }

    @Override
    public void setLastUpdated(@Nullable final LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public @Nullable String getOrganizationName() {
        return this.organizationName;
    }

    @Override
    public void setOrganizationName(@Nullable final String organizationName) {
        this.organizationName = organizationName;
    }
}
