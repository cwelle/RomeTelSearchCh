package com.rometools.modules.searchch;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.eclipse.jdt.annotation.NonNull;
import org.jdom2.Element;
import org.jdom2.Namespace;

import com.rometools.modules.searchch.interfaces.SearchCH;
import com.rometools.rome.feed.module.Module;
import com.rometools.rome.io.ModuleParser;

/**
 * @author Charlotte
 *
 */
public class SearchCHParser implements ModuleParser {

    private static final Namespace SEARCH_CH_NS = Namespace.getNamespace("tel", SearchCH.URI);
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Override
    public String getNamespaceUri() {
        return SearchCH.URI;
    }

    @Override
    public Module parse(final Element dcRoot, final Locale arg1) {
        boolean foundSomething = false;
        final SearchCH searchCH = new SearchCHImpl();

        Element e = dcRoot.getChild(SearchCHConstants.NAME, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setName(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.ZIP, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setZip(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.TITLE, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setTitle(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.ORG, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setOrganizationName(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.SUBNAME, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setSubName(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.FIRSTNAME, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setFirstName(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.MAIDENNAME, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setMaidenName(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.OCCUPATION, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setOccupation(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.STREET, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setStreet(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.STREETNO, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setStreetNumber(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.POBOX, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setPOBox(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.CITY, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setCity(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.CANTON, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setCanton(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.NOPROMO, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setNoPromo(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.PHONE, SEARCH_CH_NS);
        if (e != null) {
            foundSomething = true;
            searchCH.setPhone(e.getText());
        }
        e = dcRoot.getChild(SearchCHConstants.PUBLISHED, SEARCH_CH_NS);
        if (e != null) {
            final String value = e.getText();
            if (value != null) {
                final LocalDate localDate = LocalDate.parse(value, DATE_FORMAT);

                searchCH.setPublished(localDate);
                foundSomething = true;
            }
        }
        e = dcRoot.getChild(SearchCHConstants.UPDATED, SEARCH_CH_NS);
        if (e != null) {
            final String value = e.getText();
            if (value != null) {
                final LocalDate localDate = LocalDate.parse(value, DATE_FORMAT);

                searchCH.setLastUpdated(localDate);
                foundSomething = true;
            }
        }
        e = dcRoot.getChild(SearchCHConstants.POS, SEARCH_CH_NS);
        if (e != null) {
            final String value = e.getText();
            if (value != null) {
                try {
                    final Integer pos = Integer.decode(value);
                    searchCH.setPos(pos.intValue());
                    foundSomething = true;
                } catch (final NumberFormatException e1) {
                    // Nothing to do
                }
            }

        }
        e = dcRoot.getChild(SearchCHConstants.ID, SEARCH_CH_NS);
        if (e != null) {
            final String value = e.getText();
            if (value != null) {
                try {
                    final Integer id = Integer.decode(value);
                    searchCH.setId(id.intValue());
                    foundSomething = true;
                } catch (final NumberFormatException e1) {
                    // Nothing to do
                }
            }

        }
        List<Element> eList = dcRoot.getChildren(SearchCHConstants.CATEGORY, SEARCH_CH_NS);
        if (eList.size() > 0) {
            foundSomething = true;
            searchCH.setCategories(parseCategories(eList));
        }
        eList = dcRoot.getChildren(SearchCHConstants.EXTRA, SEARCH_CH_NS);
        if (eList.size() > 0) {
            foundSomething = true;
            parseExtras(eList, searchCH);
        }

        return (foundSomething) ? searchCH : null;
    }

    @NonNull
    private List<@NonNull String> parseCategories(@NonNull final List<Element> eList) {
        final List<@NonNull String> result = new ArrayList<>();
        for (int i = 0; i < eList.size(); i++) {
            final Element e = eList.get(i);
            final String text = e.getText();
            if (text != null) {
                result.add(text);
            }
        }
        return result;
    }

    @NonNull
    private List<@NonNull String> parseExtras(@NonNull final List<Element> eList, @NonNull final SearchCH searchCH) {
        final List<@NonNull String> result = new ArrayList<>();
        for (int i = 0; i < eList.size(); i++) {
            final Element e = eList.get(i);
            final String type = e.getAttributeValue(SearchCHConstants.TYPE);
            if (SearchCHConstants.FAX.equals(type)) {
                final String text = e.getText();
                searchCH.setFax(text);
            } else if (SearchCHConstants.EMAIL.equals(type)) {
                final String text = e.getText();
                searchCH.setEmail(text);
            } else if (SearchCHConstants.MOBILE.equals(type)) {
                final String text = e.getText();
                searchCH.setMobile(text);
            } else if (SearchCHConstants.SKYPE.equals(type)) {
                final String text = e.getText();
                searchCH.setSkype(text);
            } else if (SearchCHConstants.WEBSITE.equals(type)) {
                final String text = e.getText();
                searchCH.setWebSite(text);
            }
        }
        return result;
    }
}
