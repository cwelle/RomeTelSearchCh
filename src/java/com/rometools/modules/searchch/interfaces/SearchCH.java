package com.rometools.modules.searchch.interfaces;

import java.time.LocalDate;
import java.util.List;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;

import com.rometools.rome.feed.module.Module;

/**
 * Interface of Search.ch module.
 *
 * @author Welle Charlotte
 */
public interface SearchCH extends Module {

    /**
     * URI namespace.
     */
    public static final String URI = "http://tel.search.ch/api/spec/result/1.0/";

    /**
     * Get pos.
     *
     * @return pos
     *         -1 if no pos can be found
     */
    public int getPos();

    /**
     * Set pos.
     *
     * @param pos
     */
    public void setPos(int pos);

    /**
     * Get unique tel.search.ch identification of the entry.
     *
     * @return unique tel.search.ch identification of the entry
     *         -1 if no id can be found
     */
    public int getId();

    /**
     * Set unique tel.search.ch identification of the entry.
     *
     * @param id
     */
    public void setId(int id);

    /**
     * Get name.
     *
     * @return name
     */
    @Nullable
    public String getName();

    /**
     * Set name.
     *
     * @param name
     */
    public void setName(@Nullable String name);

    /**
     * Get type: Person or Organization.
     *
     * @return type
     */
    @Nullable
    public String getType();

    /**
     * Set type.
     *
     * @param type
     */
    public void setType(@Nullable String type);

    /**
     * Get firstname of the person.
     *
     * @return first name
     */
    @Nullable
    public String getFirstName();

    /**
     * Set firstname of the person.
     *
     * @param firstName
     */
    public void setFirstName(@Nullable String firstName);

    /**
     * Get additional name of the person.
     *
     * @return additional name
     */
    @Nullable
    public String getSubName();

    /**
     * Set additional name of the person.
     *
     * @param subName additional name
     */
    public void setSubName(@Nullable String subName);

    /**
     * Get maiden name of the person.
     *
     * @return maiden name
     */
    @Nullable
    public String getMaidenName();

    /**
     * Set maiden name of the person.
     *
     * @param maiden maiden name
     */
    public void setMaidenName(@Nullable String maiden);

    /**
     * Get occupation of the person.
     *
     * @return occupation
     */
    @Nullable
    public String getOccupation();

    /**
     * Set occupation of the person.
     *
     * @param occupation occupation
     */
    public void setOccupation(@Nullable String occupation);

    /**
     * Get category of the business entry (multiple elements possible).
     *
     * @return occupation
     */
    @Nullable
    public List<@NonNull String> getCategories();

    /**
     * Set category of the business entry (multiple elements possible).
     *
     * @param categories categories
     */
    public void setCategories(@Nullable List<@NonNull String> categories);

    /**
     * Get street of the person.
     *
     * @return occupation
     */
    @Nullable
    public String getStreet();

    /**
     * Set street of the person.
     *
     * @param street street
     */
    public void setStreet(@Nullable String street);

    /**
     * Get street number of the person.
     *
     * @return street number
     */
    @Nullable
    public String getStreetNumber();

    /**
     * Set street number of the person.
     *
     * @param number street number
     */
    public void setStreetNumber(@Nullable String number);

    /**
     * Get P.O.Box address of the person.
     *
     * @return P.O.Box address
     */
    @Nullable
    public String getPOBox();

    /**
     * Set P.O.Box address.
     *
     * @param box P.O.Box address
     */
    public void setPOBox(@Nullable String box);

    /**
     * Get city of the person.
     *
     * @return city
     */
    @Nullable
    public String getCity();

    /**
     * Set city.
     *
     * @param city city
     */
    public void setCity(@Nullable String city);

    /**
     * Get abbreviation of canton.
     *
     * @return abbreviation of canton (ZH,BE,AG,...)
     */
    @Nullable
    public String getCanton();

    /**
     * Set abbreviation of canton.
     *
     * @param canton canton
     */
    public void setCanton(@Nullable String canton);

    /**
     * Get "* No promotions please".
     *
     * @return "* No promotions please"
     */
    @Nullable
    public String getNoPromo();

    /**
     * Set "* No promotions please".
     *
     * @param noPromo "* No promotions please"
     */
    public void setNoPromo(@Nullable String noPromo);

    /**
     * Get phone number with area code.
     *
     * @return phone number with area code
     */
    @Nullable
    public String getPhone();

    /**
     * Set phone number with area code.
     *
     * @param phone phone number with area code
     */
    public void setPhone(@Nullable String phone);

    /**
     * Get fax.
     *
     * @return fax
     */
    @Nullable
    public String getFax();

    /**
     * Set fax.
     *
     * @param fax fax
     */
    public void setFax(@Nullable String fax);

    /**
     * Get email.
     *
     * @return email
     */
    @Nullable
    public String getEmail();

    /**
     * Set email.
     *
     * @param email email
     */
    public void setEmail(@Nullable String email);

    /**
     * Get website URL.
     *
     * @return website
     */
    @Nullable
    public String getWebSite();

    /**
     * Set website URL.
     *
     * @param website website URL
     */
    public void setWebSite(@Nullable String website);

    /**
     * Get skype user name.
     *
     * @return skype
     */
    @Nullable
    public String getSkype();

    /**
     * Set skype.
     *
     * @param skype skype user name
     */
    public void setSkype(@Nullable String skype);

    /**
     * Get mobile number.
     *
     * @return mobile
     */
    @Nullable
    public String getMobile();

    /**
     * Set mobile.
     *
     * @param mobile mobile number
     */
    public void setMobile(@Nullable String mobile);

    /**
     * Get zip.
     *
     * @return zip
     */
    @Nullable
    public String getZip();

    /**
     * Set zip.
     *
     * @param zip
     */
    public void setZip(@Nullable String zip);

    /**
     * Get title (name of the person or business).
     *
     * @return title (name of the person or business)
     */
    @Nullable
    public String getTitle();

    /**
     * Set title (name of the person or business).
     *
     * @param title (name of the person or business)
     */
    public void setTitle(@Nullable String title);

    /**
     * Get organization name.
     *
     * @return organization name
     */
    @Nullable
    public String getOrganizationName();

    /**
     * Set organization name.
     *
     * @param organizationName organization name
     */
    public void setOrganizationName(@Nullable String organizationName);

    /**
     * Get date of publication.
     *
     * @return date
     */
    @Nullable
    public LocalDate getPublished();

    /**
     * Set date of publication.
     *
     * @param date date of publication
     */
    public void setPublished(@Nullable LocalDate date);

    /**
     * Get last changes date.
     *
     * @return last changes date
     */
    @Nullable
    public LocalDate getLastUpdated();

    /**
     * Set last changes date.
     *
     * @param lastUpdated last changes date
     */
    public void setLastUpdated(@Nullable LocalDate lastUpdated);

}
