/**
 * This package contains Modules for working with tel.search.ch
 *
 * @author Welle Charlotte
 */
package com.rometools.modules.searchch;
